# ui-test


## Assignment
This assignment has no real life purpous other than testing a few font end techniques mainly focusing on Vue.js, Vuex, Vue router and some CSS (sass/scss). There is no single correct solution, it merely functions as a base for discussion.  
In short, the assignment consists of a few parts; layout, navigation/routing, fetching data "asynchronously", presenting the data as a table.

### Layout

* All content should be in a 1000 pixels wide container in the middle of the window.
* If the window has a smaller width than 1032 pixels the container should cover the whole width of the window with a 16 pixel margin to the left and right.
* The left part of the container (25% or 250 pixels wide) should function as a sidebar.
* Move the the navigation links in the App.vue file to the sidebar.
* Styling of the layout is up to you, except the font
* Use font "Open Sans" as font for everything - https://www.fontsquirrel.com/fonts/open-sans
* Put all styling in one or several .scss files

Layout mockup

![Layout](./src/assets/layout.png)

### Routing/Navigation

* The assignment should be accessible in the /test URL path
* The navigation links should be placed in the sidebar
* Show the router output in the main content container


### Fetching data
* Create a function that returns a Promise with hardo hard-coded JSON data. To simulate an API call.
```json
[
    { "id": "Row1", "a": 234, "b": null, "deleted": false },
    { "id": "Row2", "a": 223, "b": "16/05/2018 02:55 pm", "deleted": false },
    { "id": "Row3", "a": 234, "b": null, "deleted": false },
    { "id": "Row4", "a": 234, "b": "foo", "deleted": true }
]
```

### Table
* Create a table that looks similar to th image below containing the data from the function above
* Every other row should have a white background and the others should be a light gray background color
* When a row is hovered the background should be changed to a darker gray and text in the cells should be white
* If the deleted property is true, the row should not be presented.

![Table](./src/assets/table2.png)

### Optional - possible enhancements
* When the URL is /test/1 - row 1 should always be highlighted the same style as when hovering.
* Add a delay to the async data fetch function to simulate latency.
* In the sidebar there should also be a reset-button.
* The button in the sidebar resets the table content and fetches the data and fills the table again.

---

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

